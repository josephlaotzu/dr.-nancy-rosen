<?php
/**
 * The template for displaying 404 error page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	  <div class="content">

			<article id="post-0" class="post error404 no-results not-found">
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'This is somewhat embarrassing, isn&rsquo;t it?', 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for.', 'twentytwelve' ); ?></p>
					<?php //get_search_form(); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-0 -->

		</div>
<div class="sidebar">
    <div class="border_right">
<?php get_sidebar(); ?>
	</div>
</div>
<div class="clear"></div>
<?php get_footer(); ?>