<?php get_header('home'); ?>
                <div class="content">
                	
                		<div class="homecotent">
                    	<div class="left featureimg"><img src="<?php  bloginfo('template_directory');?>/images/dr_img_home.png" width="179" height="232" /></div>
                		<div class="left w385 hometxt">
                        	<h1>Dr.  Nancy Rosen</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas rutrum aliquet fermentum. Aliquam justo est, euismod a rutrum vel, eleifend sed nibh. Quisque sed elit nec neque gravida molestie. Ut arcu felis, consequat vel lacinia sed, tempus a orci. Morbi in felis diam. Curabitur laoreet tellus et orci congue dignissim. </p>
                            <div class="readmore"><a href="#">read more <span> > </span></a></div>	
                        </div>
                        <div class="clear"></div>
                    </div>
                    	
                        <div class="carousel_bottom">
                         	<h1>MEMBERSHIPS/ORGS/AWARDS</h1>
                           <ul id="membershipcarousel" class="jcarousel-skin-tango">
                           <?php
							 $args = array( 'post_type' => 'sliderhome', 'posts_per_page' => 1 );
							$loop = new WP_Query( $args );
	    				?>
						<?php
                            while ( $loop->have_posts() ) : $loop->the_post();
                        ?>
                        <li><?php post_thumbnail(full); ?></li>            
             		     <?php endwhile;		?>                               
                           </ul>
                        </div>
                </div>
                <?php get_sidebar(); ?>
                <div class="clear"></div>
                <?php get_footer(); ?>