<?php
/**
 * Template Name: beforeafter
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
	<div class="content">
		<article>
        	<div class="entry-header">
       		 <h1>Before & After</h1>
             </div>
        <div class="entry-content">
        
        <?php 
		$id=$post->ID; 
		$post = get_page($id); 
		$content = apply_filters('the_content', $post->post_content); 
		echo $content;  
		?>
		<?php
			 $args = array( 'post_type' => 'beforeafter', 'posts_per_page' => 10 );
			$loop = new WP_Query( $args );
		?>
           <div class="beforeaftercontent"> 
				   <?php
					    while ( $loop->have_posts() ) : $loop->the_post();
                        echo '<h3>';
                        the_title();
                        echo '</h3>';
                   ?> 
                          
           <div class="before left w170">    
                 <?php if(class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'before-image'); endif; ?>
            <label>Before</label>
           </div>
           <div class="before left w170">    
                <?php if(class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'after-image'); endif; ?>
                <label>After</label>
           </div>
            
           <?php endwhile;		?>
        </div>
        
        
        </div>  
   </article>
 </div>
<div class="sidebar">
    <div class="border_right">
<?php get_sidebar(); ?>
	</div>
</div>
<div class="clear"></div>
<?php get_footer(); ?>