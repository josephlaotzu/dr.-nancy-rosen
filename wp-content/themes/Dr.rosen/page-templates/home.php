<?php
/**
 * Template Name: home
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

 get_header('home'); ?>

	<div class="row">
	<div class="col-md-8">
                	
                		<div class="homecotent">
                    	<div class="left featureimg">
                        
                        <img src="<?php echo ot_get_option( 'homecontentimg' ); ?>" width="179" height="232" /></div>
                		<div class="left w385 hometxt">
                        	<h1><?php echo ot_get_option( 'homecontenttitle' ); ?></h1>
                            <?php echo ot_get_option( 'homecontenttext' ); ?>                          
                            <div class="readmore"><a href="<?php echo ot_get_option( 'homecontentreadmore' ); ?>">read more <span> > </span></a></div>	
                        </div>
                        <div class="clear"></div>
                    </div>
                    	
                        <div class="carousel_bottom member hidden-xs">
                         	<h1>MEMBERSHIPS/ORGS/AWARDS</h1>
                           <ul id="membershipcarousel" class="jcarousel-skin-tango">
                           <?php
							 $args = array( 'post_type' => 'logocarousel', 'posts_per_page' => 10 );
							$loop = new WP_Query( $args );
	    				?>
						<?php
                            while ( $loop->have_posts() ) : $loop->the_post();
                        ?>
                        <li><?php the_post_thumbnail(full); ?></li>            
             		     <?php endwhile;		?>                               
                           </ul>
                        </div>
                </div>
<div class="col-md-4">
    <div class="border_right">
<?php get_sidebar(); ?>
	</div>
</div>
</div>
<?php get_footer(); ?>