<?php
/**
 * Template Name: Testimonial
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div class="content">
		<article>
		<header class="entry-header">
			<h1 class="entry-title"><?php the_title(); ?></h1>
		</header>

		<div class="entry-content">
			<?php
			 $args = array( 'post_type' => 'testimonials', 'posts_per_page' => 10 );
			$loop = new WP_Query( $args );
	    	?>
			<?php
				while ( $loop->have_posts() ) : $loop->the_post();
            ?>
               <div id="ivycat-testimonial">  
                     <div class="ict-content">
                     	 <?php the_content(); ?>             
                      </div>
                     <footer class="footer-cite">
                        <cite><?php the_title(); ?></cite>
                     </footer>
                     <div class="clear"></div>
              </div>
             <?php endwhile; ?>
             
           
            
            
		</div><!-- .entry-content -->
		
	</article><!-- #post -->

        
        
   </div>
<div class="sidebar">
    <div class="border_right">
<?php get_sidebar(); ?>
	</div>
</div>
<div class="clear"></div>
<?php get_footer(); ?>