<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<?php wp_head(); ?>
<link href="<?php  bloginfo('template_directory');?>/css/style.css" rel="stylesheet" />
<!--  jCarousel library -->
<script type="text/javascript" src="<?php  bloginfo('template_directory');?>/js/jquery.jcarousel.min.js"></script>
<!--   jCarousel skin stylesheet -->
<link rel="stylesheet" type="text/css" href="<?php  bloginfo('template_directory');?>/css/skin.css" />
<link rel="stylesheet" type="text/css" href="<?php  bloginfo('template_directory');?>/css/memberskin.css" />

<!-- bxSlider Javascript file -->
<script src="<?php  bloginfo('template_directory');?>/js/jquery.bxslider.min.js"></script>
<!-- bxSlider CSS file -->
<link href="<?php  bloginfo('template_directory');?>/css/jquery.bxslider.css" rel="stylesheet" />

<script type="text/javascript">

function mycarousel_initCallback(carousel)
{
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
        auto: 0,
        wrap: 'last',
        initCallback: mycarousel_initCallback
    });
	 jQuery('#membershipcarousel').jcarousel({
        auto: 0,
        wrap: 'last',
        initCallback: mycarousel_initCallback
    });
	
     jQuery('.bxslider').bxSlider({
  auto: true,
  autoControls: true
});
	
});
</script>

</head>
<body <?php body_class(); ?>>
	<div class="head-bg">
        	<div class="wrapper12">
                    <div id="nav">
                        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                            <div class="container">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="<?php bloginfo('url'); ?>"><i class="glyphicon glyphicon-home"></i></a>
                                    <!--<a class="navbar-brand" href="<?php bloginfo('url'); ?>"><img class="logo" src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="" /></a>-->
                                </div>


                                <?php
                                                require_once("wp_bootstrap_navwalker.php");
                                                wp_nav_menu( array(
                                                        'theme_location'  => 'primary',
                                                        'menu'            => '',
                                                        'depth'             => 0,
                                                        'container'         => 'div',
                                                        'container_class'   => 'navbar-collapse collapse',
                                                        'menu_class'        => 'nav navbar-nav',
                                                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                                        'walker'            => new wp_bootstrap_navwalker())
                                                    );
                            ?>
                            </div>
                            <!-- /.container-fluid -->
                        </nav>
                    </div>     
        </div>    
        <div class="slider_bg hidden-xs"> 
        	<div class="wrapper12"> 
            	<div class="wrapper960">
            		<div class="slider_content">
                   		 <h1><?php echo ot_get_option( 'slidertitle' ); ?></h1>
                     	 <?php echo ot_get_option( 'slidercontent' ); ?>                       
               		 </div>  
                <div class="clear"></div>
                <div class="carousel_slider">
                 <ul id="mycarousel" class="jcarousel-skin-tango">
                     <?php
							 $args = array( 'post_type' => 'slidercarousel', 'posts_per_page' => 6 );
							$loop = new WP_Query( $args );
	    				?>
						<?php
                            while ( $loop->have_posts() ) : $loop->the_post();
                        ?>
                        <li><a href="/in-print/"><?php the_post_thumbnail(full); ?></a></li>            
             		     <?php endwhile;		?>          
               </ul>
              	 
                  <label>Skilled Dentistry for a Beautiful Smile</label>                  
                </div>
                </div>        
                <div class="" style="width:703px; right:-74px; float:right;">     
                     <ul class="bxslider">
                      <?php
							 $args = array( 'post_type' => 'sliderhome', 'posts_per_page' => 10 );
							$loop = new WP_Query( $args );
	    				?>
						<?php
                            while ( $loop->have_posts() ) : $loop->the_post();
                        ?>
                        <li><?php the_post_thumbnail(full); ?></li>            
             		     <?php endwhile;		?>
                     </ul>  
                 </div> 
                 
             </div>
        </div>        
        <div class="clear"></div>              			
    </div>
    <div class="content_main">
    	<div class="content_wrapper wrapper12">
        	<div class="wrapper960">