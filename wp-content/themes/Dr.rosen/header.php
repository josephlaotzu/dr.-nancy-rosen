<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?php wp_title( '|', true, 'right' ); ?></title>

<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="<?php  bloginfo('template_directory');?>/css/style.css" rel="stylesheet" />

  <!--<script type="text/javascript" src="<?php  bloginfo('template_directory');?>/js/jquery-1.9.1.min.js"></script>-->

<?php wp_head(); ?>
<script type="text/javascript">
var mojaba = {
	domain: 'http://m.drnancyrosen.com',
	fullSiteDelayMinutes: 10
}
</script>
<script type="text/javascript" src="https://c776917.ssl.cf2.rackcdn.com/includes/mojaba-mobile-redirect.js"></script>

</head>

<body <?php body_class(); ?>>

	<div class="head-bg">

        	<div class="wrapper12">
            	<div class="wrapper960">
                    <div id="nav">
                        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                            <div class="container">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="<?php bloginfo('url'); ?>"><i class="glyphicon glyphicon-home"></i></a>

                                    <!--<a class="navbar-brand" href="<?php bloginfo('url'); ?>"><img class="logo" src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="" /></a>-->
                                </div>


                                <?php
                                                require_once("wp_bootstrap_navwalker.php");
                                                wp_nav_menu( array(
                                                        'theme_location'  => 'primary',
                                                        'menu'            => '',
                                                        'depth'             => 0,
                                                        'container'         => 'div',
                                                        'container_class'   => 'navbar-collapse collapse',
                                                        'menu_class'        => 'nav navbar-nav',
                                                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                                        'walker'            => new wp_bootstrap_navwalker())
                                                    );
                            ?>
                            </div>
                            <!-- /.container-fluid -->
                        </nav>
                    </div>
                    <!-- #nav -->
                	 <?php                    

                     /*
                    $defaults = array(

                        'theme_location'  => 'primary',
                        'menu'            => '',
                        'container'       => 'div',
                        'container_class' => '',
                        'container_id'    => 'access',
                        'menu_class'      => 'menu',
                        'menu_id'         => '',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 0,
                        'walker'          => ''
                    );                    

                    wp_nav_menu( $defaults );                    
*/
                    ?>

                    <div class="clear"></div>                                                         	

                </div>     


        </div>    

        <div class="banner_bg"> 

        	<div class="wrapper12"> 

            	<div class="wrapper960">

            		<div class="logo">

                    <h1> <?php echo ot_get_option( 'logosubpageheding' ); ?>    </h1>

                        <p> <?php echo ot_get_option( 'logosubpagetxt' ); ?>     </p>                        

                </div>  

                <div class="clear"></div>

                </div>        

             </div>

        </div>        

        <div class="clear"></div>              			

    </div>

    <div class="content_sub_main">

    	<div class="content_wrapper wrapper12">

        	<div class="wrapper960">

               