</div>
</div>

<div class="footer-home">
    <div class="row">
        <div class="col-sm-4" style="display:block;margin:0 auto;">
            <?php
	 		 $defaults = array(
                        'theme_location'  => 'Footer',
                        'menu'            => '',
                        'container'       => 'div',
                        'container_class' => 'menu-footer',
                        'container_id'    => '',
                        'menu_class'      => '',
                        'menu_id'         => '',
                        'echo'            => true,
                        'fallback_cb'     => 'wp_page_menu',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth'           => 0,
                        'walker'          => ''
                    );                    
                    wp_nav_menu( $defaults );
           ?>
        </div>
        <div class="col-sm-8" style="display:block;margin:0 auto;">
            <div class="copyright">
                <div class="addy" itemscope itemtype="http://schema.org/LocalBusiness">
                    <span class="year">©<?php echo date('Y'); ?></span>
                    <a itemprop="url" href="http://drnancyrosen.com/">
                        <div itemprop="name"><strong>Nancy M. Rosen, D.M.D. PLLC</strong></div>
                    </a>
                    <br />
                    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <span itemprop="streetAddress">30 East 60th Street, Suite #408,</span>
                        <span itemprop="addressLocality">New York,</span>
                        <span itemprop="addressRegion">NY,</span>
                        <span itemprop="postalCode">10022,</span>
                        <span itemprop="addressCountry">USA</span>
                    </div>
                    Phone: <span itemprop="telephone">212-888-8327</span>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
</div>

<?php wp_footer(); ?>
    </body>

    </html>